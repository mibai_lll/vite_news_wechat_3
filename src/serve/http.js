import axios from 'axios'

export const http = axios.create({
  baseURL: '',
  timeout: 10000,
  headers: { 'X-Requested-With': 'XMLHttpRequest' },
  withCredentials: false
})

export default {
  http,
  install(app) {
    app.config.globalProperties.$http = http
  }
}
