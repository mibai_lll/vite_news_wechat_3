import axios from './axios'
//请求示例
//post
export const userLogin = (data) => {
  return axios({
    url: 'userLogin.php',
    method: 'post',
    data,
    config: {
      timeout: 6000
    }
  })
}
