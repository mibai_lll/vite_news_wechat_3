// http/index.js
import axios from 'axios'

import { Toast } from 'vant'

//创建axios的一个实例
var instance = axios.create({
  baseURL: import.meta.env.VITE_APP_URL, //接口统一域名
  timeout: 6000, //设置超时
  headers: {
    'Content-Type': 'application/json;charset=UTF-8;'
  }
})
//请求拦截器
instance.interceptors.request.use(
  (config) => {
    Toast.loading({
      message: '加载中...',
      forbidClick: true,
      loadingType: 'spinner'
    })
    //若请求方式为post，则将data参数转为JSON字符串
    if (config.method === 'POST') {
      config.data = JSON.stringify(config.data)
    }
    return config
  },
  (error) =>
    // 对请求错误做些什么
    Promise.reject(error)
)

//响应拦截器
instance.interceptors.response.use(
  (response) => {
    Toast.clear()
    //响应成功
    // console.log(response);
    let resCode = response.data.responseCode || response.status
    if (resCode != 1000 && resCode != 0 && resCode != 200) {
      Toast(response.data.responseMsg + '😟')
    }
    // if(response && response.data && response.data.indexOf('10012') > -1) {
    //   Toast("超过每日请求限制😟");
    // }
    return response
  },
  (error) => {
    Toast.clear()
    console.log(error)
    //响应错误
    if (error.response && error.response.status) {
      const status = error.response.status
      let message = ''
      switch (status) {
        case 400:
          message = '请求错误'
          break
        case 401:
          message = '请求错误'
          break
        case 404:
          message = '请求地址出错'
          break
        case 408:
          message = '请求超时'
          break
        case 500:
          message = '服务器内部错误!'
          break
        case 501:
          message = '服务未实现!'
          break
        case 502:
          message = '网关错误!'
          break
        case 503:
          message = '服务不可用!'
          break
        case 504:
          message = '网关超时!'
          break
        case 505:
          message = 'HTTP版本不受支持'
          break
        default:
          message = '请求失败'
      }
      console.log(message)
      return Promise.reject(error)
    }
    return Promise.reject(error)
  }
)

export default instance
