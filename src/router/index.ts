import { createRouter, createWebHashHistory, RouteRecordRaw } from 'vue-router'
import Home from '../views/Home/Home.vue'
import Login from '../views/Login/login.vue'

const routes:Array<RouteRecordRaw> = [
  {
    path:'/Home',
    name:'Home',
    component:Home,
    meta: {
      title: '米-新闻',//页面title
      login: false//是否需要登陆
    }        
  },
  {//重定向
    path:'/',
    redirect: 'Home'    
  },{
    path:'/Login',
    name:'Login',
    component:Login,
    meta: {
      title: '登陆',
      login: false
    }        
  },
]
const router = createRouter({
  history: createWebHashHistory(),
  routes
})
//动态修改页面title
router.beforeEach((to) => {
  (<any>document).title = to.meta.title
})
export default router