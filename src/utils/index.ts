import { Toast } from 'vant'

const utils = {
  ui: {
    Toast: (msg:string) => {
      Toast(msg)
    }
  }
}

export default utils