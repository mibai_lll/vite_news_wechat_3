declare module '*.vue' {
  import { App, defineComponent } from 'vue'
  const component: ReturnType<typeof defineComponent> & {
    install(app: App): void
  }
  export default component

}

// declare module 'i18nPlugin'

declare interface Window {
  Vue: any,
  '*.vue': any,
  $i18n: {
    locale:string,
    locales:{code:string, name:string, file:string}[]
  }
}