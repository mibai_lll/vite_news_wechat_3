import { createApp } from 'vue'
import router from './router'
import App from './App.vue'

import Vant from 'vant'
import 'vant/lib/index.css'


import utils from './utils'
(<any>window).utils = utils

import VConsole from 'vconsole'

if(import.meta.env.VITE_MODULE == 'prod') {
  let vConsole = new VConsole()
}

createApp(App)
  .use(router)
  .use(Vant)
.mount('#app')
