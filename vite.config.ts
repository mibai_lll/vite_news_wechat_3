import { defineConfig } from "vite"
import vue from '@vitejs/plugin-vue'
import viteCompression from 'vite-plugin-compression'
export default defineConfig({
  plugins: [
    vue()
  ],
  base: "./",
  css: {
    preprocessorOptions: {
      scss: {
        additionalData:
          '@import "./src/styles/hack.scss";'
      }
    }
  }
})